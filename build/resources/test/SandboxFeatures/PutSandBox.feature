Feature: Update Sandbox API entries

Scenario: I call put on a previously created Sandbox
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  Then I call put request on previously created Sandbox
  Then the response should contain http code '200'
  Then the response should contain the correct details for previously checked sandbox name

Scenario: I call put on a previously created Sandbox
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  Then I call put request on previously created Sandbox with custom description: 'test'
  Then the response should contain http code '200'
  Then the response should contain the correct details for previously checked sandbox name

Scenario: I call put by existing Sandbox name
  #PositiveScenario
  Given I make post request with Sandbox Name: 'racerxxx111'
  Then the response should contain http code '200'
  Given I call put request on Sandbox name: 'racerxxx111'
  Then the response should contain http code '200'
  Then the response should contain the correct details for previously checked sandbox name

Scenario: I call put by non existing Sandbox name
  #NegativeScenario
  Given I call put request on Sandbox name: 'xxx'
  Then the response should contain http code '400'
  And the response error message should contain 'Failed to retrieve sandbox'