Feature: Post Sandbox API entries

Scenario: I call post on the Sandbox API with a new Sandbox entry
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  Then I call get request on previously created Sandbox name
  Then the response should contain http code '200'
  Then the response should contain the correct details for previously checked sandbox name

Scenario: I call post on the Sandbox API with accepted custom name
  #PositiveScenario
  Given I make post request with Sandbox Name: 'racer1'
  Then the response should contain http code '200'
  Then I call get request on previously created Sandbox name
  Then the response should contain http code '200'
  Then the response should contain the correct details for name: 'racer1'
  Then I call delete request on previously created Sandbox

Scenario: I call post on the Sandbox API unaccepted custom name
  #NegativeScenario
  Given I make post request with Sandbox Name: '@#!$^&%&*12312542151531'
  Then the response should contain http code '400'
  And the response error message should contain 'Name has invalid characters'










