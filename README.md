# Sandbox TestAPI

Hello, 
setup was made with Serenity, Cucumber as test framework base and Gradle as automation tool for the required project tasks.

As a mock API for this test I've used https://any-api.com/getsandbox_com/getsandbox_com/docs/API_Description mainly due to the fact that it's a general purpose API (the only one I could find in there) which makes good use of all CRUD operations.

The API Documentation is located in the root folder of the project as APIEndpointsDocumentation text file.

How I did it:

I started a new project in IntelliJ for which I selected Gradle as project task manager (Maven xmls are ugly :D), added the required Cucumber, Serenity, etc. plugins and packages in the build.gradle config file which can be seen in the root folder.

After creating all the objects needed, methods involved and feature files in the tree structure of the project, of course, I needed to run the tests, therefore I added a Runner Class in the root of the steps folder in order to properly see the output of the tests. (can be ran from Gradle run test tasks but it does not offer output for the positive cases)

If anyone wants to write down extra tests the neccessary steps involved are:

- Be sure to have the right objects for the operation (as a trick, you can use Postman requests to find out how objects look like from a json response);
- Create valid methods around these objects depending on what the requirement is;
- Be sure to glue the methods to correctly mirrored feature stories

The general course for this action would be (let's say we have a new PATCH operation):

- Be sure to have the needed request object for this one;
- Create a new PATCH method in the ApiHelper class which can be mirrored from the PUT one;
- Create a new PATCHSandboxSteps class where to call that ApiHelper method and add all the neccessary extra details to the method (such as getting a previously created Sandbox entry name in order to have an entry to patch)
- Add Gherkin annotation to the SandboxAPISteps class which details the operation ("I make a patch request to SandboxAPI")
- Create a feature file for this PATCH feature where you detail the positive and negative scenarios and execute the above created methods via Gherkin annotations.
