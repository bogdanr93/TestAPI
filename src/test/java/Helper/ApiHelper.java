package Helper;

import Sandbox.Objects.ErrorMessageObj;
import Sandbox.Objects.SandboxObj;
import Sandbox.Objects.SandboxResponseObj;
import io.restassured.http.Header;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import static io.restassured.RestAssured.given;

public class ApiHelper {

    @Step
    public SandboxResponseObj Post(String path, SandboxObj body, Header header) {
        Response response = given().contentType("application/json")
                .header(header)
                .body(body)
                .when().post(path)
                .then().extract().response();
        SandboxResponseObj result = response.as(SandboxResponseObj.class);
        ErrorMessageObj errorMessage = response.as(ErrorMessageObj.class);
        Serenity.getCurrentSession().put("httpCode", response.statusCode());
        Serenity.getCurrentSession().put("errorMessage", errorMessage);
        Serenity.getCurrentSession().put("body", response.as(SandboxResponseObj.class));
        return result;
    }

    @Step
    public SandboxResponseObj Get(String path, Header header) {
        Response response = given().contentType("application/json")
        .given().header(header).get(path).then().extract().response();
        SandboxResponseObj result = response.as(SandboxResponseObj.class);
        ErrorMessageObj errorMessage = response.as(ErrorMessageObj.class);
        Serenity.getCurrentSession().put("httpCode", response.statusCode());
        Serenity.getCurrentSession().put("errorMessage", errorMessage);
        Serenity.getCurrentSession().put("body", response.as(SandboxResponseObj.class));
        return result;
    }

    @Step
    public SandboxResponseObj Put(String path, SandboxObj body, Header header) {
        Response response = given().contentType("application/json")
                .header(header)
                .body(body)
                .when().put(path)
                .then().extract().response();
        SandboxResponseObj result = response.as(SandboxResponseObj.class);
        ErrorMessageObj errorMessage = response.as(ErrorMessageObj.class);
        Serenity.getCurrentSession().put("httpCode", response.statusCode());
        Serenity.getCurrentSession().put("errorMessage", errorMessage);
        return result;
    }

    @Step
    public SandboxResponseObj Delete(String path, Header header) {
        Response response = given().contentType("application/json")
                .header(header)
                .when().delete(path)
                .then().extract().response();
        SandboxResponseObj result = response.as(SandboxResponseObj.class);
        ErrorMessageObj errorMessage = response.as(ErrorMessageObj.class);
        Serenity.getCurrentSession().put("httpCode", response.statusCode());
        Serenity.getCurrentSession().put("errorMessage", errorMessage);
        return result;
    }
}
