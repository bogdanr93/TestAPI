package Sandbox.Steps.Serenity;

import Sandbox.Objects.ErrorMessageObj;
import Sandbox.Objects.SandboxResponseObj;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

public class AssertionSteps extends ScenarioSteps {

    @Step
    public static void assertSandboxHttpcode(String httpCodeInput) {
        var httpCode = Serenity.getCurrentSession().get("httpCode").toString();
        Assert.assertEquals(httpCodeInput, httpCode);
    }

    @Step
    public static void assertSandboxMessage(String errorMessageInput) {
        ErrorMessageObj errorResponseObj = (ErrorMessageObj) Serenity.getCurrentSession().get("errorMessage");
        var errorMessage = errorResponseObj.errors.get(0);
        System.out.println(errorMessage.message);
        Assert.assertEquals(errorMessageInput, errorResponseObj.errors.get(0).message);
    }

    @Step
    public static void assertSandboxMembersName(String name) {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        Assert.assertEquals(responseObj.getName(), name);
        Assert.assertEquals(responseObj.getDescription(), "Wow");
        Assert.assertEquals(responseObj.getHasRepository(), true);
        Assert.assertEquals(responseObj.getApiDefinition(), "None");
        Assert.assertEquals(responseObj.getStackType(), "JavaScript");
        Assert.assertEquals(responseObj.getRuntimeVersion(), "VERSION_2");
        Assert.assertEquals(responseObj.getProxyStatus(), "STARTED");
        Assert.assertEquals(responseObj.getTransportType(), "HTTP");
    }

    public static void assertSandboxMembers() {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        Assert.assertEquals(responseObj.getName(), responseObj.getName());
        Assert.assertEquals(responseObj.getDescription(), "Wow");
        Assert.assertEquals(responseObj.getHasRepository(), true);
        Assert.assertEquals(responseObj.getApiDefinition(), "None");
        Assert.assertEquals(responseObj.getStackType(), "JavaScript");
        Assert.assertEquals(responseObj.getRuntimeVersion(), "VERSION_2");
        Assert.assertEquals(responseObj.getProxyStatus(), "STARTED");
        Assert.assertEquals(responseObj.getTransportType(), "HTTP");
    }
}

