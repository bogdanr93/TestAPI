package Sandbox.Steps.Serenity;

import Helper.ApiHelper;
import Sandbox.Objects.SandboxObj;
import Sandbox.Objects.SandboxResponseObj;
import io.restassured.http.Header;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class PutSandboxSteps extends ScenarioSteps {

    public static final Header HEADER = new Header("API-Key", "api-61a6820b-f2c2-4cae-8ab6-f5cb9b7a886f");
    public static final String getSandboxesPath = "https://getsandbox.com/api/1/sandboxes";

    @Step
    public static void updatePreviouslyCreatedSandbox() {
        var random = Math.random() * 9000000;
        var round = Math.round(random);
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        SandboxObj obj = new SandboxObj("testboxxxx" + round, "Wow", true, "None", "JavaScript", "VERSION_2", "STARTED", "HTTP", "http://git.getsandbox.com/testbox.git", "http://testbox.getsandbox.com");
        var response = new ApiHelper().Put(getSandboxesPath + "/" + responseObj.getName(), obj, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void updateSandboxByName(String customName) {
        var random = Math.random() * 9000000;
        var round = Math.round(random);
        SandboxObj obj = new SandboxObj("testboxxxx" + round, "Wow", true, "None", "JavaScript", "VERSION_2", "STARTED", "HTTP", "http://git.getsandbox.com/testbox.git", "http://testbox.getsandbox.com");
        var response = new ApiHelper().Put(getSandboxesPath + "/" + customName, obj, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void updatePreviouslyCreatedSandboxWithCustomDescription(String customDescription) {
        var random = Math.random() * 9000000;
        var round = Math.round(random);
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        SandboxObj obj = new SandboxObj("testboxxxx" + round, customDescription, true, "None", "JavaScript", "VERSION_2", "STARTED", "HTTP", "http://git.getsandbox.com/testbox.git", "http://testbox.getsandbox.com");
        var response = new ApiHelper().Put(getSandboxesPath + "/" + responseObj.getName(), obj, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }
}