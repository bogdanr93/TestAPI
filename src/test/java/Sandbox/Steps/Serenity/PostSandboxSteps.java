package Sandbox.Steps.Serenity;

import Helper.ApiHelper;
import Sandbox.Objects.SandboxObj;
import io.restassured.http.Header;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class PostSandboxSteps extends ScenarioSteps {

    public static final Header HEADER = new Header("API-Key", "api-61a6820b-f2c2-4cae-8ab6-f5cb9b7a886f");
    public static final String getSandboxesPath = "https://getsandbox.com/api/1/sandboxes";

    @Step
    public static void createNewSandbox() {
        var random = Math.random() * 100000;
        var round = Math.round(random);
        SandboxObj obj = new SandboxObj("testboxxxx" + round, "Wow", true, "None", "JavaScript", "VERSION_2", "STARTED", "HTTP", "http://git.getsandbox.com/testbox.git", "http://testbox.getsandbox.com");
        var response = new ApiHelper().Post(getSandboxesPath, obj, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void createNewSandboxWithName(String customName) {
        SandboxObj obj = new SandboxObj(customName, "Wow", true, "None", "JavaScript", "VERSION_2", "STARTED", "HTTP", "http://git.getsandbox.com/testbox.git", "http://testbox.getsandbox.com");
        var response = new ApiHelper().Post(getSandboxesPath, obj, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }
}