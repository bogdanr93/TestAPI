package Sandbox.Steps.Serenity;

import Helper.ApiHelper;
import Sandbox.Objects.SandboxResponseObj;
import io.restassured.http.Header;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class GetSandboxSteps extends ScenarioSteps {

    public static final Header HEADER = new Header("API-Key", "api-61a6820b-f2c2-4cae-8ab6-f5cb9b7a886f");
    public static final String getSandboxesPath = "https://getsandbox.com/api/1/sandboxes";
    public static final String getActivitySearch = "https://getsandbox.com/api/1/activity/search?";

    @Step
    public static void getCreatedSandboxes() {
        var response = new ApiHelper().Get(getSandboxesPath, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void getSearchActivity(String queryParam, String queryParamValue) {
        var response = new ApiHelper().Get(getActivitySearch + queryParam + "=" + queryParamValue, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void getCreatedSandboxesWithName(String customName) {
        var response = new ApiHelper().Get(getSandboxesPath + "/" + customName, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void getCreatedSandboxesWithInvalidAPIKEYAuth() {
        var response = new ApiHelper().Get(getSandboxesPath, new Header("API-Key" ,"1"));
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void getPreviouslyCreatedSandbox() {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        var response = new ApiHelper().Get(getSandboxesPath + "/" + responseObj.getName(), HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    public static void forkPreviouslyCreatedSandbox() {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        var response = new ApiHelper().Get(getSandboxesPath + "/" + responseObj.getName() + "/fork", HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    public static void getPreviouslyCreatedSandboxState() {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        var response = new ApiHelper().Get(getSandboxesPath + "/" + responseObj.getName() + "/state", HEADER);
        Serenity.getCurrentSession().put("response", response);
    }
}