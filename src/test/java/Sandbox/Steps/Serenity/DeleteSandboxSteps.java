package Sandbox.Steps.Serenity;

import Helper.ApiHelper;
import Sandbox.Objects.SandboxResponseObj;
import io.restassured.http.Header;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class DeleteSandboxSteps extends ScenarioSteps {

    public static final Header HEADER = new Header("API-Key", "api-61a6820b-f2c2-4cae-8ab6-f5cb9b7a886f");
    public static final String getSandboxesPath = "https://getsandbox.com/api/1/sandboxes";

    @Step
    public static void deletePreviouslyCreatedSandbox() {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        var response = new ApiHelper().Delete(getSandboxesPath + "/" + responseObj.getName(), HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void deleteSandboxByName(String customName) {
        var response = new ApiHelper().Delete(getSandboxesPath + "/" + customName, HEADER);
        Serenity.getCurrentSession().put("response", response);
    }

    @Step
    public static void deletePreviouslyCreatedSandboxState() {
        SandboxResponseObj responseObj = (SandboxResponseObj) Serenity.getCurrentSession().get("body");
        var response = new ApiHelper().Delete(getSandboxesPath + "/" + responseObj.getName() +"/state", HEADER);
        Serenity.getCurrentSession().put("response", response);
    }
}