package Sandbox.Steps;

import Sandbox.Steps.Serenity.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class SandboxAPISteps extends ScenarioSteps {

    @Steps
    public GetSandboxSteps getSandboxSteps;
    public PostSandboxSteps postSandboxSteps;
    public PutSandboxSteps putSandboxSteps;
    public DeleteSandboxSteps deleteSandboxSteps;
    public AssertionSteps assertionSteps;

    @Given("I make get request on GetSandboxAPI")
    public void getSandboxes() {
        getSandboxSteps.getCreatedSandboxes();
    }

    @Given("I make get request on Search Activity with queryParam: {string} and with value: {string}")
    public void getSearchActivity(String queryParam, String queryParamValue) {
        getSandboxSteps.getSearchActivity(queryParam, queryParamValue);
    }

    @Given("I call get request on GetSandbox by name: {string}")
    public void iCallGetRequestOnGetSandboxByName(String customName) {
        getSandboxSteps.getCreatedSandboxesWithName(customName);
    }

    @Given("I call get request on GetSandboxAPI with invalid API KEY auth")
    public void getSandboxesWithNoAPIKEY() {
        getSandboxSteps.getCreatedSandboxesWithInvalidAPIKEYAuth();
    }

    @Given("I make post request on SandboxAPI")
    public void postSandboxes() {
        postSandboxSteps.createNewSandbox();
    }

    @Given("I make post request with Sandbox Name: {string}")
    public void postSandboxesWithName(String customName) {
        postSandboxSteps.createNewSandboxWithName(customName);
    }

    @Then("I call get request on previously created Sandbox name")
    public void iCallGetRequestOnPreviouslyCreatedSandboxName() {
        getSandboxSteps.getPreviouslyCreatedSandbox();
    }

    @Then("I call get fork request on previously created Sandbox name")
    public void iCallForkRequestOnPreviouslyCreatedSandboxName() {
        getSandboxSteps.forkPreviouslyCreatedSandbox();
    }

    @Then("I call get state request on previously created Sandbox")
    public void iCallGetRequestOnPreviouslyCreatedSandboxState() {
        getSandboxSteps.getPreviouslyCreatedSandboxState();
    }

    @Then("I call put request on previously created Sandbox")
    public void iCallPutRequestOnPreviouslyCreatedSandboxName(){
        putSandboxSteps.updatePreviouslyCreatedSandbox();
    }

    @Then("I call put request on Sandbox name: {string}")
    public void iCallPutRequestByName(String customName){
        putSandboxSteps.updateSandboxByName(customName);
    }

    @Then("I call put request on previously created Sandbox with custom description: {string}")
    public void updatePreviouslyCreatedSandboxWithCustomDescription(String customDescription){
        putSandboxSteps.updatePreviouslyCreatedSandboxWithCustomDescription(customDescription);
    }

    @Then("I call delete request on previously created Sandbox")
    public void iCallDeleteRequestOnPreviouslyCreatedSandboxName(){
        deleteSandboxSteps.deletePreviouslyCreatedSandbox();
    }

    @Then("I call delete request by Sandbox name: {string}")
    public void iCallDeleteRequestbySandboxName(String customName){
        deleteSandboxSteps.deleteSandboxByName(customName);
    }

    @Then("I call delete state request on previously created Sandbox")
    public void iCallDeleteStateRequestOnPreviouslyCreatedSandboxName(){
        deleteSandboxSteps.deletePreviouslyCreatedSandboxState();
    }

    @Then("the response should contain http code {string}")
    public void theResponseShouldContainHttpCode(String httpCodeInput) {
        assertionSteps.assertSandboxHttpcode(httpCodeInput);
    }

    @Then("the response error message should contain {string}")
    public void assertSandboxMessage(String errorMessageInput) {
        assertionSteps.assertSandboxMessage(errorMessageInput);
    }

    @Then("the response should contain the correct details for name: {string}")
    public void theResponseShouldContainTheFollowing(String name) {
        assertionSteps.assertSandboxMembersName(name);
    }

    @Then("the response should contain the correct details for previously checked sandbox name")
    public void theResponseShouldContainTheFollowing() {
        assertionSteps.assertSandboxMembers();
    }
}
