package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "transport",
        "body",
        "headers",
        "cookies",
        "responded_timestamp",
        "duration_ms",
        "status"
})
public class ResponseObj {

    @JsonProperty("transport")
    private String transport;
    @JsonProperty("body")
    private String body;
    @JsonProperty("headers")
    private Headers_Obj headers;
    @JsonProperty("cookies")
    private List<Object> cookies = null;
    @JsonProperty("responded_timestamp")
    private Integer respondedTimestamp;
    @JsonProperty("duration_ms")
    private Integer durationMs;
    @JsonProperty("status")
    private Integer status;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("transport")
    public String getTransport() {
        return transport;
    }

    @JsonProperty("transport")
    public void setTransport(String transport) {
        this.transport = transport;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty("headers")
    public Headers_Obj getHeaders() {
        return headers;
    }

    @JsonProperty("headers")
    public void setHeaders(Headers_Obj headers) {
        this.headers = headers;
    }

    @JsonProperty("cookies")
    public List<Object> getCookies() {
        return cookies;
    }

    @JsonProperty("cookies")
    public void setCookies(List<Object> cookies) {
        this.cookies = cookies;
    }

    @JsonProperty("responded_timestamp")
    public Integer getRespondedTimestamp() {
        return respondedTimestamp;
    }

    @JsonProperty("responded_timestamp")
    public void setRespondedTimestamp(Integer respondedTimestamp) {
        this.respondedTimestamp = respondedTimestamp;
    }

    @JsonProperty("duration_ms")
    public Integer getDurationMs() {
        return durationMs;
    }

    @JsonProperty("duration_ms")
    public void setDurationMs(Integer durationMs) {
        this.durationMs = durationMs;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}