package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "createdDate",
        "createdDayOfMonth",
        "country",
        "plan"
})
public class BillingObj {

    @JsonProperty("id")
    private String id;
    @JsonProperty("createdDate")
    private Integer createdDate;
    @JsonProperty("createdDayOfMonth")
    private Integer createdDayOfMonth;
    @JsonProperty("country")
    private String country;
    @JsonProperty("plan")
    private PlanObj plan;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("createdDate")
    public Integer getCreatedDate() {
        return createdDate;
    }

    @JsonProperty("createdDate")
    public void setCreatedDate(Integer createdDate) {
        this.createdDate = createdDate;
    }

    @JsonProperty("createdDayOfMonth")
    public Integer getCreatedDayOfMonth() {
        return createdDayOfMonth;
    }

    @JsonProperty("createdDayOfMonth")
    public void setCreatedDayOfMonth(Integer createdDayOfMonth) {
        this.createdDayOfMonth = createdDayOfMonth;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("plan")
    public PlanObj getPlan() {
        return plan;
    }

    @JsonProperty("plan")
    public void setPlan(PlanObj plan) {
        this.plan = plan;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}