package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "sandboxAllowance",
        "requestAllowance",
        "userAllowance",
        "cost",
        "annual"
})
public class PlanObj {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("sandboxAllowance")
    private Integer sandboxAllowance;
    @JsonProperty("requestAllowance")
    private Integer requestAllowance;
    @JsonProperty("userAllowance")
    private Integer userAllowance;
    @JsonProperty("cost")
    private Integer cost;
    @JsonProperty("annual")
    private Boolean annual;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("sandboxAllowance")
    public Integer getSandboxAllowance() {
        return sandboxAllowance;
    }

    @JsonProperty("sandboxAllowance")
    public void setSandboxAllowance(Integer sandboxAllowance) {
        this.sandboxAllowance = sandboxAllowance;
    }

    @JsonProperty("requestAllowance")
    public Integer getRequestAllowance() {
        return requestAllowance;
    }

    @JsonProperty("requestAllowance")
    public void setRequestAllowance(Integer requestAllowance) {
        this.requestAllowance = requestAllowance;
    }

    @JsonProperty("userAllowance")
    public Integer getUserAllowance() {
        return userAllowance;
    }

    @JsonProperty("userAllowance")
    public void setUserAllowance(Integer userAllowance) {
        this.userAllowance = userAllowance;
    }

    @JsonProperty("cost")
    public Integer getCost() {
        return cost;
    }

    @JsonProperty("cost")
    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @JsonProperty("annual")
    public Boolean getAnnual() {
        return annual;
    }

    @JsonProperty("annual")
    public void setAnnual(Boolean annual) {
        this.annual = annual;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}