
package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "transport",
        "sandboxId",
        "sandboxName",
        "fullSandboxName",
        "fullSandboxId",
        "headers",
        "properties",
        "body",
        "ip",
        "cookies",
        "query",
        "accepted",
        "url",
        "params",
        "method",
        "path",
        "rawUrl",
        "received_timestamp"
})
public class RequestObj {

    @JsonProperty("transport")
    private String transport;
    @JsonProperty("sandboxId")
    private String sandboxId;
    @JsonProperty("sandboxName")
    private String sandboxName;
    @JsonProperty("fullSandboxName")
    private String fullSandboxName;
    @JsonProperty("fullSandboxId")
    private String fullSandboxId;
    @JsonProperty("headers")
    private HeadersObj headers;
    @JsonProperty("properties")
    private PropertiesObj properties;
    @JsonProperty("body")
    private String body;
    @JsonProperty("ip")
    private String ip;
    @JsonProperty("cookies")
    private CookiesObj cookies;
    @JsonProperty("query")
    private QueryObj query;
    @JsonProperty("accepted")
    private List<String> accepted = null;
    @JsonProperty("url")
    private String url;
    @JsonProperty("params")
    private ParamsObj params;
    @JsonProperty("method")
    private String method;
    @JsonProperty("path")
    private String path;
    @JsonProperty("rawUrl")
    private String rawUrl;
    @JsonProperty("received_timestamp")
    private Integer receivedTimestamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("transport")
    public String getTransport() {
        return transport;
    }

    @JsonProperty("transport")
    public void setTransport(String transport) {
        this.transport = transport;
    }

    @JsonProperty("sandboxId")
    public String getSandboxId() {
        return sandboxId;
    }

    @JsonProperty("sandboxId")
    public void setSandboxId(String sandboxId) {
        this.sandboxId = sandboxId;
    }

    @JsonProperty("sandboxName")
    public String getSandboxName() {
        return sandboxName;
    }

    @JsonProperty("sandboxName")
    public void setSandboxName(String sandboxName) {
        this.sandboxName = sandboxName;
    }

    @JsonProperty("fullSandboxName")
    public String getFullSandboxName() {
        return fullSandboxName;
    }

    @JsonProperty("fullSandboxName")
    public void setFullSandboxName(String fullSandboxName) {
        this.fullSandboxName = fullSandboxName;
    }

    @JsonProperty("fullSandboxId")
    public String getFullSandboxId() {
        return fullSandboxId;
    }

    @JsonProperty("fullSandboxId")
    public void setFullSandboxId(String fullSandboxId) {
        this.fullSandboxId = fullSandboxId;
    }

    @JsonProperty("headers")
    public HeadersObj getHeaders() {
        return headers;
    }

    @JsonProperty("headers")
    public void setHeaders(HeadersObj headers) {
        this.headers = headers;
    }

    @JsonProperty("properties")
    public PropertiesObj getProperties() {
        return properties;
    }

    @JsonProperty("properties")
    public void setProperties(PropertiesObj properties) {
        this.properties = properties;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty("ip")
    public String getIp() {
        return ip;
    }

    @JsonProperty("ip")
    public void setIp(String ip) {
        this.ip = ip;
    }

    @JsonProperty("cookies")
    public CookiesObj getCookies() {
        return cookies;
    }

    @JsonProperty("cookies")
    public void setCookies(CookiesObj cookies) {
        this.cookies = cookies;
    }

    @JsonProperty("query")
    public QueryObj getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(QueryObj query) {
        this.query = query;
    }

    @JsonProperty("accepted")
    public List<String> getAccepted() {
        return accepted;
    }

    @JsonProperty("accepted")
    public void setAccepted(List<String> accepted) {
        this.accepted = accepted;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("params")
    public ParamsObj getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(ParamsObj params) {
        this.params = params;
    }

    @JsonProperty("method")
    public String getMethod() {
        return method;
    }

    @JsonProperty("method")
    public void setMethod(String method) {
        this.method = method;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    @JsonProperty("rawUrl")
    public String getRawUrl() {
        return rawUrl;
    }

    @JsonProperty("rawUrl")
    public void setRawUrl(String rawUrl) {
        this.rawUrl = rawUrl;
    }

    @JsonProperty("received_timestamp")
    public Integer getReceivedTimestamp() {
        return receivedTimestamp;
    }

    @JsonProperty("received_timestamp")
    public void setReceivedTimestamp(Integer receivedTimestamp) {
        this.receivedTimestamp = receivedTimestamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}