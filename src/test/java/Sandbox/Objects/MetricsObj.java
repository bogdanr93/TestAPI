package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "forks",
        "clones",
        "requests",
        "gitUpdateDate"
})
public class MetricsObj {

    @JsonProperty("forks")
    private Integer forks;
    @JsonProperty("clones")
    private Integer clones;
    @JsonProperty("requests")
    private Integer requests;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("forks")
    public Integer getForks() {
        return forks;
    }

    @JsonProperty("forks")
    public void setForks(Integer forks) {
        this.forks = forks;
    }

    @JsonProperty("clones")
    public Integer getClones() {
        return clones;
    }

    @JsonProperty("clones")
    public void setClones(Integer clones) {
        this.clones = clones;
    }

    @JsonProperty("requests")
    public Integer getRequests() {
        return requests;
    }

    @JsonProperty("requests")
    public void setRequests(Integer requests) {
        this.requests = requests;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}