
package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "createdTimestamp",
        "messageType",
        "sandboxId",
        "messageObject"
})
public class ActivityMessageObj {

    @JsonProperty("createdTimestamp")
    private Integer createdTimestamp;
    @JsonProperty("messageType")
    private String messageType;
    @JsonProperty("sandboxId")
    private String sandboxId;
    @JsonProperty("messageObject")
    private MessageObjectObj messageObject;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("createdTimestamp")
    public Integer getCreatedTimestamp() {
        return createdTimestamp;
    }

    @JsonProperty("createdTimestamp")
    public void setCreatedTimestamp(Integer createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    @JsonProperty("messageType")
    public String getMessageType() {
        return messageType;
    }

    @JsonProperty("messageType")
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @JsonProperty("sandboxId")
    public String getSandboxId() {
        return sandboxId;
    }

    @JsonProperty("sandboxId")
    public void setSandboxId(String sandboxId) {
        this.sandboxId = sandboxId;
    }

    @JsonProperty("messageObject")
    public MessageObjectObj getMessageObject() {
        return messageObject;
    }

    @JsonProperty("messageObject")
    public void setMessageObject(MessageObjectObj messageObject) {
        this.messageObject = messageObject;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

