
package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "request",
        "responses"
})
public class MessageObjectObj {

    @JsonProperty("request")
    private RequestObj request;
    @JsonProperty("responses")
    private List<ResponseObj> responses = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("request")
    public RequestObj getRequest() {
        return request;
    }

    @JsonProperty("request")
    public void setRequest(RequestObj request) {
        this.request = request;
    }

    @JsonProperty("responses")
    public List<ResponseObj> getResponses() {
        return responses;
    }

    @JsonProperty("responses")
    public void setResponses(List<ResponseObj> responses) {
        this.responses = responses;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}