package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "description",
        "ownerOrganisation",
        "commitBaseTemplate",
        "hasRepository",
        "apiDefinition",
        "stackType",
        "runtimeVersion",
        "metrics",
        "proxyStatus",
        "transportType",
        "gitUrl",
        "sandboxUrl"
})
public class SandboxResponseObj {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("ownerOrganisation")
    private OwnerOrganisationObj ownerOrganisation;
    @JsonProperty("commitBaseTemplate")
    private Boolean commitBaseTemplate;
    @JsonProperty("hasRepository")
    private Boolean hasRepository;
    @JsonProperty("apiDefinition")
    private String apiDefinition;
    @JsonProperty("stackType")
    private String stackType;
    @JsonProperty("runtimeVersion")
    private String runtimeVersion;
    @JsonProperty("metrics")
    private MetricsObj metrics;
    @JsonProperty("proxyStatus")
    private String proxyStatus;
    @JsonProperty("transportType")
    private String transportType;
    @JsonProperty("gitUrl")
    private String gitUrl;
    @JsonProperty("sandboxUrl")
    private String sandboxUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("ownerOrganisation")
    public OwnerOrganisationObj getOwnerOrganisation() {
        return ownerOrganisation;
    }

    @JsonProperty("ownerOrganisation")
    public void setOwnerOrganisation(OwnerOrganisationObj ownerOrganisation) {
        this.ownerOrganisation = ownerOrganisation;
    }

    @JsonProperty("commitBaseTemplate")
    public Boolean getCommitBaseTemplate() {
        return commitBaseTemplate;
    }

    @JsonProperty("commitBaseTemplate")
    public void setCommitBaseTemplate(Boolean commitBaseTemplate) {
        this.commitBaseTemplate = commitBaseTemplate;
    }

    @JsonProperty("hasRepository")
    public Boolean getHasRepository() {
        return hasRepository;
    }

    @JsonProperty("hasRepository")
    public void setHasRepository(Boolean hasRepository) {
        this.hasRepository = hasRepository;
    }

    @JsonProperty("apiDefinition")
    public String getApiDefinition() {
        return apiDefinition;
    }

    @JsonProperty("apiDefinition")
    public void setApiDefinition(String apiDefinition) {
        this.apiDefinition = apiDefinition;
    }

    @JsonProperty("stackType")
    public String getStackType() {
        return stackType;
    }

    @JsonProperty("stackType")
    public void setStackType(String stackType) {
        this.stackType = stackType;
    }

    @JsonProperty("runtimeVersion")
    public String getRuntimeVersion() {
        return runtimeVersion;
    }

    @JsonProperty("runtimeVersion")
    public void setRuntimeVersion(String runtimeVersion) {
        this.runtimeVersion = runtimeVersion;
    }

    @JsonProperty("metrics")
    public MetricsObj getMetrics() {
        return metrics;
    }

    @JsonProperty("metrics")
    public void setMetrics(MetricsObj metrics) {
        this.metrics = metrics;
    }

    @JsonProperty("proxyStatus")
    public String getProxyStatus() {
        return proxyStatus;
    }

    @JsonProperty("proxyStatus")
    public void setProxyStatus(String proxyStatus) {
        this.proxyStatus = proxyStatus;
    }

    @JsonProperty("transportType")
    public String getTransportType() {
        return transportType;
    }

    @JsonProperty("transportType")
    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    @JsonProperty("gitUrl")
    public String getGitUrl() {
        return gitUrl;
    }

    @JsonProperty("gitUrl")
    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    @JsonProperty("sandboxUrl")
    public String getSandboxUrl() {
        return sandboxUrl;
    }

    @JsonProperty("sandboxUrl")
    public void setSandboxUrl(String sandboxUrl) {
        this.sandboxUrl = sandboxUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}