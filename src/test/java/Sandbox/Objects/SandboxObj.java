package Sandbox.Objects;

import lombok.Data;

@Data
public class SandboxObj {

    public String name;
    public String description;
    public boolean hasRepository;
    public String apiDefinition;
    public String stackType;
    public String runtimeVersion;
    public String proxyStatus;
    public String transportType;
    public String gitUrl;
    public String sandboxUrl;

    public SandboxObj(String name,
                      String description,
                      boolean hasRepository,
                      String apiDefinition,
                      String stackType,
                      String runtimeVersion,
                      String proxyStatus,
                      String transportType,
                      String gitUrl,
                      String sandboxUrl) {
        this.name = name;
        this.description = description;
        this.hasRepository = hasRepository;
        this.apiDefinition = apiDefinition;
        this.stackType = stackType;
        this.runtimeVersion = runtimeVersion;
        this.proxyStatus = proxyStatus;
        this.transportType = transportType;
        this.gitUrl = gitUrl;
        this.sandboxUrl = sandboxUrl;
    }
}



