package Sandbox.Objects;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OwnerOrganisationObj {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "id",
            "name",
            "billingEmail",
            "billingUserId",
            "billing",
            "teams",
            "users",
            "connectedServices"
    })
    public class Example {

        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("billingEmail")
        private String billingEmail;
        @JsonProperty("billingUserId")
        private String billingUserId;
        @JsonProperty("billing")
        private BillingObj billing;
        @JsonProperty("teams")
        private List<Object> teams = null;
        @JsonProperty("users")
        private List<Object> users = null;
        @JsonProperty("connectedServices")
        private List<Object> connectedServices = null;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("id")
        public String getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("billingEmail")
        public String getBillingEmail() {
            return billingEmail;
        }

        @JsonProperty("billingEmail")
        public void setBillingEmail(String billingEmail) {
            this.billingEmail = billingEmail;
        }

        @JsonProperty("billingUserId")
        public String getBillingUserId() {
            return billingUserId;
        }

        @JsonProperty("billingUserId")
        public void setBillingUserId(String billingUserId) {
            this.billingUserId = billingUserId;
        }

        @JsonProperty("billing")
        public BillingObj getBilling() {
            return billing;
        }

        @JsonProperty("billing")
        public void setBilling(BillingObj billing) {
            this.billing = billing;
        }

        @JsonProperty("teams")
        public List<Object> getTeams() {
            return teams;
        }

        @JsonProperty("teams")
        public void setTeams(List<Object> teams) {
            this.teams = teams;
        }

        @JsonProperty("users")
        public List<Object> getUsers() {
            return users;
        }

        @JsonProperty("users")
        public void setUsers(List<Object> users) {
            this.users = users;
        }

        @JsonProperty("connectedServices")
        public List<Object> getConnectedServices() {
            return connectedServices;
        }

        @JsonProperty("connectedServices")
        public void setConnectedServices(List<Object> connectedServices) {
            this.connectedServices = connectedServices;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}
