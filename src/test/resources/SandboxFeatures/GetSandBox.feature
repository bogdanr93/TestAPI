Feature: Get Sandbox API entries

Scenario: I call GetSandboxes API and I should receive a 500 response (not working API)
  #PositiveScenario #NotWorkingAPI
  Given I make get request on GetSandboxAPI
  Then the response should contain http code '500'
  And the response error message should contain 'Sorry, an error has occurred. Please check your request data and try again.'

Scenario: I call get request on a previously created Sandbox
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  When I call get request on previously created Sandbox name
  Then the response should contain http code '200'
  Then the response should contain the correct details for previously checked sandbox name

Scenario: I call fork on a previously created Sandbox (not working API)
  #PositiveScenario #NotWorkingAPI
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  When I call get fork request on previously created Sandbox name
  Then the response should contain http code '500'
  And the response error message should contain 'Sorry, an error has occurred. Please check your request data and try again.'

Scenario: I call get state on a previously created Sandbox
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  When I call get state request on previously created Sandbox
  Then the response should contain http code '200'

Scenario: I call get request on a Sandbox with existing Sandbox name
  #PositiveScenario
  Given I call get request on GetSandbox by name: 'testboxx'
  Then the response should contain http code '200'
  Then the response should contain the correct details for previously checked sandbox name

Scenario: I call get request on a Sandbox with fromTimestamp query parameter (lacking sandBoxes query param)
  #NegativeScenario
  Given I make get request on Search Activity with queryParam: 'fromTimestamp' and with value: '1'
  Then the response should contain http code '400'
  And the response error message should contain 'sourceSandboxes is required'

Scenario: I call get request on a Sandbox with non existing Sandbox name
  #NegativeScenario
  Given I call get request on GetSandbox by name: 'xxx'
  Then the response should contain http code '400'
  And the response error message should contain 'Failed to retrieve sandbox'

Scenario: I call get request on a Sandbox with with invalid API KEY auth
  #NegativeScenario
  Given I call get request on GetSandboxAPI with invalid API KEY auth
  Then the response should contain http code '401'
  And the response error message should contain 'Your API Key is invalid.'