Feature: Delete Sandbox API entries

Scenario: I call DeleteSandboxes API entry
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  Then I call delete request on previously created Sandbox
  Then the response should contain http code '200'

Scenario: I call delete state on a previously created Sandbox entry
  #PositiveScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  Then I call delete state request on previously created Sandbox
  Then the response should contain http code '200'

Scenario: I call DeleteSandboxes API entry by non existing name
  #NegativeScenario
  Given I make post request on SandboxAPI
  Then the response should contain http code '200'
  Then I call delete request by Sandbox name: 'xxx'
  Then the response should contain http code '400'
  And the response error message should contain 'Failed to retrieve sandbox'